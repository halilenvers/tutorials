﻿namespace Tutorial_AmazonRDS.Models {
    public class Recipe {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
