using Tutorial_AmazonRDS.Models;
using Tutorial_AmazonRDS;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);


ConfigureServices(
    builder.Services,
    builder.Configuration
);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();


//app.UseHttpsRedirection();

app.MapGet("/recipes", async (AppDbContext db)
    => await db.Recipes.ToListAsync())
    .Produces<List<Recipe>>(StatusCodes.Status200OK);

app.MapGet("/recipes/{id:int}", async (AppDbContext db, int id)
    => await db.Recipes.FindAsync(id))
    .Produces<List<Recipe>>(StatusCodes.Status200OK);

app.MapPost("/recipes", async (AppDbContext db, Recipe recipe) => {
    await db.Recipes.AddAsync(recipe);
    await db.SaveChangesAsync();
    return Results.Created($"/recipes/{recipe.Id}", recipe);
}).Produces<List<Recipe>>(StatusCodes.Status201Created);


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment()) {
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.Run();


void ConfigureServices(IServiceCollection services, ConfigurationManager configurationManager) {
    services.AddDbContext<AppDbContext>(
        opts =>
        {
            opts.UseNpgsql(configurationManager.GetConnectionString("AppDb"));
        }, ServiceLifetime.Transient
        );
}

