﻿using Microsoft.EntityFrameworkCore;
using Tutorial_AmazonRDS.Models;

namespace Tutorial_AmazonRDS {
    public class AppDbContext : DbContext {
        public AppDbContext() {
        }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) {
        }

        public virtual DbSet<Recipe> Recipes { get; set; }
    }
}
