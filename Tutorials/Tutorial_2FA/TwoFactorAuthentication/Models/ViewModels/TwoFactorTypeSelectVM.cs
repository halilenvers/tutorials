﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TwoFactorAuthentication.Models.Enums;

namespace TwoFactorAuthentication.Models.ViewModels
{
    public class TwoFactorTypeSelectVM
    {
        public TwoFactorType TwoFactorType { get; set; }
    }
}
