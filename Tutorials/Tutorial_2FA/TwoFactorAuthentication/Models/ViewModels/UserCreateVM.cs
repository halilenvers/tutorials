﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TwoFactorAuthentication.Models.ViewModels
{
    public class UserCreateVM
    {
        [Required(ErrorMessage = "Lütfen adı boş geçmeyiniz.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Lütfen emaili boş geçmeyiniz.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Lütfen şifreyi boş geçmeyiniz.")]
        public string Password { get; set; }
    }
}
