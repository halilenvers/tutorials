﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TwoFactorAuthentication.Models.ViewModels
{
    public class UserSignVM
    {
        [Required(ErrorMessage = "Lütfen emaili boş geçmeyiniz.")]
        [StringLength(55, MinimumLength = 10, ErrorMessage = "Lütfen email adresini 10 - 55 karakter aralığında giriniz.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Lütfen şifreyi boş geçmeyiniz.")]
        public string Password { get; set; }
    }
}
