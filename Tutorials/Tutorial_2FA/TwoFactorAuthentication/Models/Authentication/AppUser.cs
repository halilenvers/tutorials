﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TwoFactorAuthentication.Models.Enums;

namespace TwoFactorAuthentication.Models.Authentication
{
    public class AppUser : IdentityUser<int>
    {
        public TwoFactorType TwoFactorType { get; set; }
    }
}
