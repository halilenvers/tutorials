﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TwoFactorAuthentication.Models.Authentication;

namespace TwoFactorAuthentication.Models
{
    public class TwoFactorAuthenticationContext : IdentityDbContext<AppUser, AppRole, int>
    {
        public TwoFactorAuthenticationContext(DbContextOptions options) : base(options)
        {
        }
    }
}
