using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TwoFactorAuthentication.Models;
using TwoFactorAuthentication.Models.Authentication;
using TwoFactorAuthentication.Services;

namespace TwoFactorAuthentication
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<TwoFactorAuthenticationContext>(o => o.UseSqlServer("Server=LAPTOP-M6GQT38I;Database=Tutorial_2FA;Trusted_Connection=True;"));

            services.AddIdentity<AppUser, AppRole>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequiredLength = 3;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
            }).AddEntityFrameworkStores<TwoFactorAuthenticationContext>().AddDefaultTokenProviders(); 
            services.ConfigureApplicationCookie(x =>
            {
                x.LoginPath = new PathString("/login/index");
                x.LogoutPath = new PathString("/login/index");
                x.AccessDeniedPath = new PathString("/user/unauthorize");
                x.ExpireTimeSpan = TimeSpan.FromMinutes(10);
                x.Cookie = new CookieBuilder
                {
                    HttpOnly = true,
                    Name = "TwoFactoryAuthentication",
                    SameSite = SameSiteMode.Lax,
                    SecurePolicy = CookieSecurePolicy.Always
                };
            });
            services.AddScoped<AuthenticatorService, AuthenticatorService>();
            services.AddControllersWithViews();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, TwoFactorAuthenticationContext dataContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            dataContext.Database.Migrate();

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}
