﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TwoFactorAuthentication.Models.Authentication;
using TwoFactorAuthentication.Models.ViewModels;

namespace TwoFactorAuthentication.Controllers
{
    public class LoginController : Controller
    {
        private readonly SignInManager<AppUser> _signInManager;
        private readonly UserManager<AppUser> _userManager;
        public LoginController(SignInManager<AppUser> signInManager, UserManager<AppUser> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
        }
        public IActionResult Index(string ReturnUrl = "")
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Index(string ReturnUrl, UserSignVM model)
        {
            if (ModelState.IsValid)
            {
                AppUser user = await _userManager.FindByEmailAsync(model.Email);
                if (user != null)
                {
                    Microsoft.AspNetCore.Identity.SignInResult result = await _signInManager.PasswordSignInAsync(user, model.Password, true, true);
                    if (result.Succeeded)
                    {
                        if (string.IsNullOrEmpty(ReturnUrl))
                            return RedirectToAction("Index", "Home");
                        return Redirect(ReturnUrl);
                    }
                    else if (result.RequiresTwoFactor)
                        return RedirectToAction("twofactorauthenticate", new { ReturnUrl = ReturnUrl });
                }
            }
            return View(model);
        }
        public async Task<IActionResult> TwoFactorAuthenticate(string ReturnUrl) => View();
        [HttpPost]
        public async Task<IActionResult> TwoFactorAuthenticate(string ReturnUrl, TwoFactorLoginVM model)
        {
            AppUser user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            Microsoft.AspNetCore.Identity.SignInResult result = null;
            if (model.Recovery)
                result = await _signInManager.TwoFactorRecoveryCodeSignInAsync(model.VerifyCode);
            else
                result = await _signInManager.TwoFactorAuthenticatorSignInAsync(model.VerifyCode, true, false);

            if (result.Succeeded)
                return Redirect(string.IsNullOrEmpty(ReturnUrl) ? "/home/index" : ReturnUrl);
            else
                ModelState.AddModelError("verifycode", "Doğrulama kodu yanlış girilmiştir!");

            return View(model);
        }
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Login");
        }
    }
}