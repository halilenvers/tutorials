﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TwoFactorAuthentication.Models.Authentication;
using TwoFactorAuthentication.Models.ViewModels;

namespace TwoFactorAuthentication.Controllers
{
    public class UserController : Controller
    {
        private readonly UserManager<AppUser> _userManager;
        public UserController(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost()]
        public async Task<IActionResult> Create(UserCreateVM model)
        {
            if (ModelState.IsValid)
            {
                AppUser user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    IdentityResult result = await _userManager.CreateAsync(new AppUser
                    {
                        UserName = model.Name,
                        Email = model.Email
                    }, model.Password);
                    if (result.Succeeded)
                    {
                        TempData["message1"] = "Kayıt başarılı. Giriş yapabilirsiniz.";
                        return RedirectToAction("Index", "Login");
                    }
                }
            }
            return View(model);
        }
    }
}