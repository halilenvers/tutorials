﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TwoFactorAuthentication.Models.Authentication;
using TwoFactorAuthentication.Models.ViewModels;
using TwoFactorAuthentication.Services;

namespace TwoFactorAuthentication.Controllers
{
    [Authorize]
    public class TwoAuthenticationController : Controller
    {
        AuthenticatorService _authenticatorService;
        UserManager<AppUser> _userManager;
        public TwoAuthenticationController(AuthenticatorService authenticatorService, UserManager<AppUser> userManager)
        {
            _authenticatorService = authenticatorService;
            _userManager = userManager;
        }

        public async Task<IActionResult> AuthenticatorVerify()
        {
            AppUser user = await _userManager.FindByNameAsync(User.Identity.Name);
            string sharedKey = await _authenticatorService.GenerateSharedKey(user);
            string qrcodeUri = await _authenticatorService.GenerateQrCodeUri(sharedKey, "www.gencayyildiz.com", user);

            return View(new AuthenticatorVM
            {
                SharedKey = sharedKey,
                QrCodeUri = qrcodeUri
            });
        }
        [HttpPost]
        public async Task<IActionResult> AuthenticatorVerify(AuthenticatorVM model)
        {
            AppUser user = await _userManager.FindByNameAsync(User.Identity.Name);
            VerifyState verifyState = await _authenticatorService.Verify(model, user);
            if (verifyState.State)
            {
                TempData["message2"] = "İki adımlı doğrulama hesaba tanımlanmıştır.";
                TempData["message3"] = verifyState.RecoveryCode;
            }
            return View(model);
        }
    }
}