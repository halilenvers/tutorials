﻿using ClosedXML.Excel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PuppeteerSharp;
using PuppeteerSharp.Media;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Tutorial_DownloadReport.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private List<WeatherForecast> forecasts = new List<WeatherForecast>
        {
            new WeatherForecast(){ Date = DateTime.Now.AddDays(0), Summary = Summaries[0], TemperatureC = 30 },
            new WeatherForecast(){ Date = DateTime.Now.AddDays(1), Summary = Summaries[1], TemperatureC = 40 },
            new WeatherForecast(){ Date = DateTime.Now.AddDays(2), Summary = Summaries[2], TemperatureC = 50 },
            new WeatherForecast(){ Date = DateTime.Now.AddDays(3), Summary = Summaries[3], TemperatureC = 60 },
            new WeatherForecast(){ Date = DateTime.Now.AddDays(4), Summary = Summaries[4], TemperatureC = 70 },
        };

        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return forecasts.ToArray();
        }

        [HttpGet]
        [Route("DownloadXLSXFile")]
        public IActionResult Excel()
        {
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Forecasts");
                var currentRow = 1;
                worksheet.Cell(currentRow, 1).Value = "Date";
                worksheet.Cell(currentRow, 2).Value = "Summary";
                worksheet.Cell(currentRow, 2).Value = "TemperatureC";
                foreach (var forecast in forecasts)
                {
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = forecast.Date;
                    worksheet.Cell(currentRow, 2).Value = forecast.Summary;
                    worksheet.Cell(currentRow, 3).Value = forecast.TemperatureC;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                        content,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "forecasts.xlsx");
                }
            }
        }

        [HttpGet]
        [Route("DownloadCSVFile")]
        public IActionResult Csv()
        {
            var builder = new StringBuilder();
            builder.AppendLine("Date,Summary,TemperatureC");
            foreach (var forecast in forecasts)
            {
                builder.AppendLine($"{forecast.Date},{forecast.Summary},{forecast.TemperatureC}");
            }

            return File(Encoding.UTF8.GetBytes(builder.ToString()), "text/csv", "forecasts.csv");
        }

        [HttpGet]
        [Route("DownloadPDFFile")]
        public async Task<IActionResult> Pdf()
        {
            await new BrowserFetcher().DownloadAsync(BrowserFetcher.DefaultRevision);
            await using var browser = await Puppeteer.LaunchAsync(new LaunchOptions
            {
                Headless = true
            });
            await using var page = await browser.NewPageAsync();
            await page.EmulateMediaTypeAsync(MediaType.Screen);
            await page.SetContentAsync("<div><h1>Hello PDF world!</h1><h2 style='color: red; text-align: center;'>Greetings from <i>HTML</i> world</h2></div>");
            var pdfContent = await page.PdfStreamAsync(new PdfOptions
            {
                Format = PaperFormat.A4,
                PrintBackground = true
            });
            return File(pdfContent, "application/pdf", "converted.pdf");
        }
    }
}
