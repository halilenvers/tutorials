﻿using System;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace Tutorial_SendSMSviaTwillo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            try
            {
                // Find your Account SID and Auth Token at twilio.com/console
                // and set the environment variables. See http://twil.io/secure
                string accountSid = "ACe1d4d64383bc0286cb7ef58f23993b48";
                string authToken = "599879c04eee4ed80fcfbb6d72bf231c";

                TwilioClient.Init(accountSid, authToken);

                var message = MessageResource.Create(
                    body: "Join Earth's mightiest heroes. Like Kevin Bacon.",
                    from: new Twilio.Types.PhoneNumber("+18507197017"),
                    to: new Twilio.Types.PhoneNumber("+905327490712")
                );
                
                Console.WriteLine(message.Sid);
            }
            catch (Exception ex)
            {
                Console.ReadLine();
            }

        }
    }
}
