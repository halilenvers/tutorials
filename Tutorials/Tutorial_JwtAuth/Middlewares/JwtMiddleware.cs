﻿using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Tutorial_JwtAuth.Entities;

namespace Tutorial_JwtAuth.Middlewares
{
    public class Constants
    {
        public static readonly byte[] JwtSecret = Guid.Empty.ToByteArray();
        public const int JwtExpireInHours = 24;
        public const string JwtIssuer = "Randegram.com";
    }

    public class JwtMiddleware
    {
        private readonly RequestDelegate _next;

        public JwtMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

            if (token != null)
                ValidateAndAttach(context, token);

            await _next(context);
        }

        public static string GenerateJwtToken(UserIdentity userIdentity)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] {
                    new Claim("id", userIdentity.Id.ToString()),
                    new Claim("name", userIdentity.Name.ToString()),
                    new Claim("email", userIdentity.Email.ToString())
                }),
                Expires = DateTime.UtcNow.AddHours(Constants.JwtExpireInHours),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Constants.JwtSecret), SecurityAlgorithms.HmacSha256Signature),

            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        private void ValidateAndAttach(HttpContext context, string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Constants.JwtSecret),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidIssuer = Constants.JwtIssuer,
                    ValidAudience = Constants.JwtIssuer,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;
                var userId = jwtToken.Claims.First(x => x.Type == "id").Value;
                var name = jwtToken.Claims.First(x => x.Type == "name").Value;
                var email = jwtToken.Claims.First(x => x.Type == "email").Value;

                List<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, userId));
                claims.Add(new Claim(ClaimTypes.Name, userId));
                claims.Add(new Claim(ClaimTypes.Email, email));
                var claimsIdentity = new ClaimsIdentity(claims, "CustomApiKeyAuth");

                ClaimsPrincipal principal = new ClaimsPrincipal();
                principal.AddIdentity(claimsIdentity);

                context.User = principal;
            }
            catch (Exception ex)
            {
                // do nothing if jwt validation fails
                // user is not attached to context so request won't have access to secure routes
            }
        }
    }

    public class JwtHelper
    {
        public const string Secret = "[SECRET USED TO SIGN AND VERIFY JWT TOKENS, IT CAN BE ANY STRING]";
        public string GenerateJwtToken(int accountId)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", accountId.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
