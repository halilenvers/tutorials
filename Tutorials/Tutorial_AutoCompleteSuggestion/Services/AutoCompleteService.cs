﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Tutorial_AutoCompleteSuggestion.Services
{
    public interface IAutocompleteService
    {
        Task<bool> CreateIndexAsync(string indexName);
        Task IndexAsync(string indexName, List<Product> products);
        Task<ProductSuggestResponse> SuggestAsync(string indexName, string keyword);
        void LoadData();
    }

    public class AutocompleteService : IAutocompleteService
    {
        public const string PRODUCT_SUGGEST_INDEX = "product_suggest";
        readonly ElasticClient _elasticClient;
        readonly ConnectionSettings _connectionSettings;

        public AutocompleteService(ConnectionSettings connectionSettings)
        {
            _connectionSettings = connectionSettings;
            _elasticClient = new ElasticClient(_connectionSettings);
        }

        public async Task<bool> CreateIndexAsync(string indexName)
        {
            var createIndexDescriptor = new CreateIndexDescriptor(indexName)
                .Mappings(ms => ms
                          .Map<Product>(m => m
                                .AutoMap()
                                .Properties(ps => ps
                                    .Completion(c => c
                                        .Name(p => p.Suggest))))

                         );

            if (_elasticClient.Indices.Exists(indexName.ToLowerInvariant()).Exists)
            {
                _elasticClient.Indices.Delete(indexName.ToLowerInvariant());
            }

            CreateIndexResponse createIndexResponse = await _elasticClient.Indices.CreateAsync(createIndexDescriptor);

            return createIndexResponse.IsValid;
        }

        public void LoadData()
        {
            List<Product> products = new List<Product>();

            products.Add(new Product()
            {
                Id = 1,
                Name = "Samsung Galaxy Note 8",
                Suggest = new CompletionField()
                {
                    Input = new[] { "Samsung Galaxy Note 8", "Galaxy Note 8", "Note 8" }
                }
            });

            products.Add(new Product()
            {
                Id = 2,
                Name = "Samsung Galaxy S8",
                Suggest = new CompletionField()
                {
                    Input = new[] { "Samsung Galaxy S8", "Galaxy S8", "S8" }
                }
            });

            products.Add(new Product()
            {
                Id = 3,
                Name = "Apple Iphone 8",
                Suggest = new CompletionField()
                {
                    Input = new[] { "Apple Iphone 8", "Iphone 8" }
                }
            });

            products.Add(new Product()
            {
                Id = 4,
                Name = "Apple Iphone X",
                Suggest = new CompletionField()
                {
                    Input = new[] { "Apple Iphone X", "Iphone X" }
                }
            });

            products.Add(new Product()
            {
                Id = 5,
                Name = "Apple iPad Pro",
                Suggest = new CompletionField()
                {
                    Input = new[] { "Apple iPad Pro", "iPad Pro" }
                }
            });

            
            IAutocompleteService autocompleteService = new AutocompleteService(_connectionSettings);

            bool isCreated = autocompleteService.CreateIndexAsync(PRODUCT_SUGGEST_INDEX).Result;

            if (isCreated)
            {
                autocompleteService.IndexAsync(PRODUCT_SUGGEST_INDEX, products).Wait();
            }
        }

        public async Task IndexAsync(string indexName, List<Product> products)
        {
            await _elasticClient.IndexManyAsync(products, indexName);
        }

        public async Task<ProductSuggestResponse> SuggestAsync(string indexName, string keyword)
        {
            ISearchResponse<Product> searchResponse = await _elasticClient.SearchAsync<Product>(s => s
                                     .Index(indexName)
                                     .Suggest(su => su
                                          .Completion("suggestions", c => c
                                               .Field(f => f.Suggest)
                                               .Prefix(keyword)
                                               .Fuzzy(f => f
                                                   .Fuzziness(Fuzziness.Auto).UnicodeAware(true).Transpositions(true)
                                               )
                                               .Size(5))
                                             ));

            var suggests = from suggest in searchResponse.Suggest["suggestions"]
                           from option in suggest.Options
                           select new ProductSuggest
                           {
                               Id = option.Source.Id,
                               Name = option.Source.Name,
                               SuggestedName = option.Text,
                               Score = option.Score
                           };

            return new ProductSuggestResponse
            {
                Suggests = suggests
            };
        }
    }
}
