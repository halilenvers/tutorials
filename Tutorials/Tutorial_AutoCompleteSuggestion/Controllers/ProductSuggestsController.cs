﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tutorial_AutoCompleteSuggestion.Services;

namespace Tutorial_AutoCompleteSuggestion.Controllers
{
    [Route("api/product-suggests")]
    public class ProductSuggestsController : Controller
    {
        readonly IAutocompleteService _autocompleteService;

        public ProductSuggestsController(IAutocompleteService autocompleteService)
        {
            _autocompleteService = autocompleteService;
        }

        [HttpGet]
        public async Task<ProductSuggestResponse> Get(string keyword)
        {
            return await _autocompleteService.SuggestAsync(AutocompleteService.PRODUCT_SUGGEST_INDEX, keyword);
        }

        [HttpGet("load")]
        public async void Load()
        {
            _autocompleteService.LoadData();
        }
    }
}
