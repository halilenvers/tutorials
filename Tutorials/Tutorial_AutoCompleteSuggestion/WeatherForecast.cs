using Nest;
using System;
using System.Collections.Generic;

namespace Tutorial_AutoCompleteSuggestion
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public CompletionField Suggest { get; set; }
    }
    public class ProductSuggestResponse
    {
        public IEnumerable<ProductSuggest> Suggests { get; set; }
    }

    public class ProductSuggest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Score { get; set; }
        public string SuggestedName { get; set; }
    }
    public class WeatherForecast
    {
        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        public string Summary { get; set; }
    }
}
