﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tutorial_MongoDBAtlas.Models;

namespace Tutorial_MongoDBAtlas.Services
{
    public class SampleDBService
    {
        private readonly IMongoCollection<User> _users;
        public SampleDBService()
        {
            string connectionString = "mongodb+srv://hestest:hes123456@cluster0.5bqo2.mongodb.net/sample_mflix?retryWrites=true&w=majority";
            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("sample_mflix");
            _users = database.GetCollection<User>("users");
        }
        public async Task<List<User>> GetAllAsync()
        {
            return await _users.Find(s => true).ToListAsync();
        }
        public async Task<User> GetByIdAsync(string id)
        {
            ObjectId _id = ObjectId.Parse(id);
            return await _users.Find<User>(s => s._id == _id).FirstOrDefaultAsync();
        }
        public async Task<User> CreateAsync(User user)
        {
            await _users.InsertOneAsync(user);
            return user;
        }
        public async Task UpdateAsync(string id, User user)
        {
            ObjectId _id = ObjectId.Parse(id);
            await _users.ReplaceOneAsync(s => s._id == _id, user);
        }
        public async Task DeleteAsync(string id)
        {
            ObjectId _id = ObjectId.Parse(id);
            await _users.DeleteOneAsync(s => s._id == _id);
        }
    }
}
