﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tutorial_MSSQL.Data.Entities;

namespace Tutorial_MSSQL.Data
{
    public class MyContext : DbContext
    {
        public DbSet<WeatherForecast> WeatherForecasts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseSqlServer(@"Data Source=LAPTOP-M6GQT38I;Initial Catalog=Tutorial_MSSQL;Trusted_Connection=True;");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WeatherForecast>().HasData(
                new WeatherForecast
                {
                     Date =DateTime.Now,
                      Id = Guid.NewGuid(),
                       Summary = Summaries.Balmy,
                        TemperatureC = 10
                },
                new WeatherForecast
                {
                    Date = DateTime.Now.AddDays(-1),
                    Id = Guid.NewGuid(),
                    Summary = Summaries.Bracing,
                    TemperatureC = 20
                }
            );;
        }
    }
}
