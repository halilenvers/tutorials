﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Tutorial_MSSQL.Data.Migrations
{
    public partial class Index_Added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "WeatherForecasts",
                columns: new[] { "Id", "Date", "Summary", "TemperatureC" },
                values: new object[] { new Guid("20a9bc63-896b-4004-9267-cdb375052951"), new DateTime(2022, 3, 18, 15, 3, 21, 347, DateTimeKind.Local).AddTicks(8688), 6, 10 });

            migrationBuilder.InsertData(
                table: "WeatherForecasts",
                columns: new[] { "Id", "Date", "Summary", "TemperatureC" },
                values: new object[] { new Guid("f2a7bf6a-889b-4391-bbbf-e236203c5b06"), new DateTime(2022, 3, 17, 15, 3, 21, 349, DateTimeKind.Local).AddTicks(3984), 1, 20 });

            migrationBuilder.CreateIndex(
                name: "IX_WeatherForecasts_Summary",
                table: "WeatherForecasts",
                column: "Summary");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_WeatherForecasts_Summary",
                table: "WeatherForecasts");

            migrationBuilder.DeleteData(
                table: "WeatherForecasts",
                keyColumn: "Id",
                keyValue: new Guid("20a9bc63-896b-4004-9267-cdb375052951"));

            migrationBuilder.DeleteData(
                table: "WeatherForecasts",
                keyColumn: "Id",
                keyValue: new Guid("f2a7bf6a-889b-4391-bbbf-e236203c5b06"));
        }
    }
}
