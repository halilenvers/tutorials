using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;

namespace Tutorial_MSSQL.Data.Entities
{
    [Index(nameof(Summary))]
    public class WeatherForecast
    {
        [Key]
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public int TemperatureC { get; set; }
        public Summaries Summary { get; set; }
    }
}
